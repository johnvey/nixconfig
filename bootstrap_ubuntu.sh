#
# my linux setup
#

#
# Pull in appropriate repos
# 

# pre-install
sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg

# setup docker repo
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# python repo
sudo add-apt-repository ppa:deadsnakes/ppa -y

# # get latest yarn
# curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
# echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

# NodeJS via NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash

#
# install packages
#

sudo apt-get update

# install docker
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo systemctl enable docker.service

# run all dpkg
sudo apt-get install -y \
  apt-transport-https \
  bluez \
  chrony \
  ffmpeg \
  fzf \
  g++ \
  git \
  libnss-mdns \
  libudev-dev \
  libyaml-dev \
  make \
  mosquitto-clients \
  samba \
  software-properties-common \
  silversearcher-ag \

# python packages
sudo apt install -y python3.12 python3.12-dev

# setup chrony
# https://blog.ubuntu.com/2018/04/09/ubuntu-bionic-using-chrony-to-configure-ntp
# also remember to tell systemd
sudo systemctl enable chrony.service
# also disable timesync, otherwise it kills chrony; some relevance: https://bugs.launchpad.net/ubuntu/+source/chrony/+bug/1779621
sudo systemctl disable systemd-timesyncd

# setup for pihole
#
# $ sudo vi /etc/systemd/resolved.conf
# DNSStubListener=no
#
# https://askubuntu.com/questions/1057723/why-do-i-need-to-add-nameservers-to-resolv-conf/1057752#1057752?newreg=bb357a3f2b0d426fa54d49e265726932
# $ sudo rm -f /etc/resolv.conf
# $ ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
#
# $ sudo systemctl start systemd-resolved
# $ sudo docker run pihole/pihole

# tailscale
curl -fsSL https://tailscale.com/install.sh | sh

#
# home automation
#

# now allow execution without sudo
sudo setcap 'cap_net_raw+ep' `which hcitool`
sudo setcap 'cap_net_admin+ep' `which hciconfig`
sudo setcap 'cap_net_raw+ep' `which hcidump`

# disable modem manager because it seems to interfere with getting Sigma zwave registered on cold boot
sudo systemctl disable ModemManager.service
