" pathogen
execute pathogen#infect()

" tab nonsense
set sts=4
set sw=4
set expandtab

" search params
set hlsearch
set incsearch

" coding niceties
set ai
syntax on
set number
filetype plugin indent on

" ag for ack.vim
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

autocmd FileType yaml setlocal shiftwidth=2 tabstop=2
