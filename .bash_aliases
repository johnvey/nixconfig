# override default sorting
alias ll='LC_COLLATE="C" ls -alF'
alias la='LC_COLLATE="C" ls -A'
alias l='LC_COLLATE="C" ls -CF'

# set make flags
MAKEFLAGS='-j 8'

# make history better
#HISTCONTROL=ignoredups:erasedups
#HISTSIZE=100000
#HISTFILESIZE=100000
#shopt -s histappend
#PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

